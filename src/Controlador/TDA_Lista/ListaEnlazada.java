
package Controlador.TDA_Lista;

import Controlador.Exceptions.EmptyListException;
import Controlador.Exceptions.ListOutLimitException;

/**
 *
 * @author Leo117
 */

public class ListaEnlazada<E> {

    private Nodo<E> cabecera;
    private Integer size;

    public ListaEnlazada() {
        cabecera = null;
        size = 0;
    }

    public Boolean isVoid() {
        return cabecera == null;
    }

    public void insertar(E dato) {
        Nodo<E> node = new Nodo<E>(dato, null);
        if (isVoid()) {
            this.cabecera = node;
        } else {
            Nodo<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(node);
        }
        this.size++;
    }

    public void insertarCabecera(E dato) {
        if (isVoid()) {
            insertar(dato);
        } else {
            Nodo<E> node = new Nodo<E>(dato, null);
            node.setSiguiente(cabecera);
            this.cabecera = node;
            this.size++;
        }
    }

    public void insertarPosition(E dato, Integer pos) throws ListOutLimitException {
        if (isVoid()) {
            insertar(dato);
        } else if (pos == 0) {
            insertarCabecera(dato);
        } else if (pos > 0 && pos < size) {
            Nodo<E> aux = cabecera;
            for (int i = 0; i < pos - 1; i++) {
                aux = aux.getSiguiente();
            }

            Nodo<E> node = new Nodo<>(dato, aux.getSiguiente());
            aux.setSiguiente(node);
            size++;

        } else if (pos == size) {
            insertar(dato);
        } else {
            throw new ListOutLimitException();
        }
    }

    public void print() {
        Nodo<E> aux = cabecera;
        System.out.println("---------------------LISTA ENLAZADA------------------");
        while (aux != null) {
            System.out.println(aux.getDato().toString() + "    ");
            aux = aux.getSiguiente();
        }
        System.out.println("-----------------------------------------------------");
    }

    public E obtener(Integer pos) throws EmptyListException, ListOutLimitException {
        E dato = null;
        if (isVoid()) {
            throw new EmptyListException();
        } else if (pos >= 0 && pos < size) {
            Nodo<E> aux = cabecera;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            dato = aux.getDato();
        } else {
            throw new ListOutLimitException();
        }
        return dato;
    }

    public void modificarPosicion(E dato, Integer pos) throws EmptyListException, ListOutLimitException {
        if (isVoid()) {
            throw new EmptyListException();
        } else if (pos >= 0 && pos < size) {
            Nodo<E> aux = cabecera;
            for (int i = 0; i < pos; i++) {
                aux = aux.getSiguiente();
            }
            aux.setDato(dato);
        } else {
            throw new ListOutLimitException();
        }
    }

    public void modificarCabecera(E dato) throws EmptyListException {
        if (isVoid()) {
            throw new EmptyListException();
        }
        cabecera.setDato(dato);
    }

    public void modificarUltimo(E dato) throws EmptyListException {
        if (isVoid()) {
            throw new EmptyListException();
        }
        Nodo<E> aux = cabecera;
        while (aux.getSiguiente() != null) {
            aux = aux.getSiguiente();
        }
        aux.setDato(dato);
    }

    public void eliminarPosicion(Integer pos) throws EmptyListException, ListOutLimitException {
        if (isVoid()) {
            throw new EmptyListException();
        } else if (pos > 0 && pos < size) {
            Nodo<E> aux = cabecera;

            for (int i = 0; i < pos - 1; i++) {
                aux = aux.getSiguiente();
            }
            Nodo<E> siguiente = aux.getSiguiente();
            aux.setSiguiente(siguiente.getSiguiente());
            siguiente.setSiguiente(null);
            size--;
        }else if(pos == 0) {
            eliminarCabecera();
        }
        else {
            throw new ListOutLimitException();
        }
    }

    public void eliminarCabecera() throws EmptyListException {
        if (isVoid()) {
            throw new EmptyListException();
        }
        if (size == 1) {
            cabecera = null;
        } else {
            Nodo<E> aux = cabecera.getSiguiente();
            cabecera.setSiguiente(null);
            cabecera = aux;
        }
        size--;
    }
    public Boolean comprobarDato(Integer j) throws Exception {
        if (!isVoid()) {
            if (j >= 0 && j < size) {
                E verificar = null;
                if (j == 0) {
                    verificar = cabecera.getDato();
                } else {
                    Nodo<E> aux = cabecera;
                    for (int i = 0; i < i; i++) {
                        aux = aux.getSiguiente();
                    }
                    verificar = aux.getDato();
                }
                return true;
            } else {
                return false;
            }
        } else {
           return false;
        }
    }

    public void eliminarUltimo() throws EmptyListException {
        if (isVoid()) {
            throw new EmptyListException();
        }

        if (size == 1) {
            eliminarCabecera();
        } else {
            Nodo<E> aux = cabecera;
            while (aux.getSiguiente().getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(null);
            size--;
        }
    }

    public Nodo<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(Nodo<E> cabecera) {
        this.cabecera = cabecera;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

}
