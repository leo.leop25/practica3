package Controlador.TDA_Grafo;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Lista.ListaEnlazada;

/**
 *
 * @author Leo117
 */
public class GrafoD extends Grafo {

    protected Integer nVertices;
    protected Integer nAristas;
    protected ListaEnlazada<Adyacencia> listaAdyacente[];

    public GrafoD(Integer numV) {
        this.nVertices = numV;
        this.nAristas = 0;
        listaAdyacente = new ListaEnlazada[numV + 1];
        for (int i = 0; i <= this.nVertices; i++) {
            listaAdyacente[i] = new ListaEnlazada<>();
        }
    }

    @Override
    public Integer nVertices() {
        return this.nVertices;
    }

    @Override
    public Integer nAristas() {
        return this.nAristas;
    }

    @Override
    public Object[] existeArista(Integer i, Integer f) throws VerticeException {
        Object[] resultado = {Boolean.FALSE, Double.NaN};
        if (i > 0 && f > 0 && i <= nVertices && f <= nVertices) {
            ListaEnlazada<Adyacencia> lista = listaAdyacente[i];
            for (int j = 0; j < lista.getSize(); j++) {
                try {
                    Adyacencia aux = lista.obtener(j);
                    if (aux.getDestino().intValue() == f.intValue()) {
                        resultado[0] = Boolean.TRUE;
                        resultado[1] = aux.getPeso();
                        break;
                    }
                } catch (Exception ex) {

                }
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
        return resultado;
    }

    @Override
    public Double pesoArista(Integer i, Integer f) throws VerticeException {
        Double peso = Double.NaN;
        Object[] existe = existeArista(i, f);
        if (((Boolean) existe[0])) {
            peso = (Double) existe[1];
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer i, Integer j) throws VerticeException {
        insertarArista(i, j, Double.NaN);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {

        if (i.intValue() <= nVertices && j.intValue() <= nVertices) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                nAristas++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }

    }

    @Override
    public ListaEnlazada<Adyacencia> adyacente(Integer i) throws VerticeException {
        return listaAdyacente[i];
    }

    public Integer getnVertices() {
        return nVertices;
    }

    public void setnVertices(Integer nVertices) {
        this.nVertices = nVertices;
    }

    public Integer getnAristas() {
        return nAristas;
    }

    public void setnAristas(Integer nAristas) {
        this.nAristas = nAristas;
    }
    
    
    public void aumentar(Integer numV, Integer numA, ListaEnlazada<Adyacencia>[] listaAdyacente) {
        this.nVertices = numV;
        this.nAristas = numA;
        this.listaAdyacente = listaAdyacente;
    }

    public void nuevoGrafo() {
        GrafoD copia = new GrafoD(nVertices() + 1);
        try {
            for (int i = 0; i < nVertices(); i++) {
                ListaEnlazada<Adyacencia> lista = adyacente(i);
                for (int j = 0; j < lista.getSize(); j++) {
                    Adyacencia aux = lista.obtener(j);
                    copia.insertarArista(i, aux.getDestino(), aux.getPeso());
                }
            }
            aumentar(copia.nVertices(), copia.nAristas(), copia.listaAdyacente);
        } catch (Exception e) {
            System.out.println("Error en aumento: " + e);
        }
    }

    public Double[][] adyacencia() throws Exception {
        Double[][] pesos = new Double[nVertices + 1][nVertices + 1];
        for (int i = 1; i <= nVertices; i++) {
            for (int j = 1; j <= nVertices; j++) {
                if (i == j) {
                    pesos[i][j] = 0.0;
                } else {
                    if (!(Boolean) existeArista(i, j)[0]) {
                        pesos[i][j] = Double.POSITIVE_INFINITY;
                    } else {
                        pesos[i][j] = pesoArista(i, j);
                    }
                }
            }
        }
        return pesos;
    }

    public ListaEnlazada<Integer> floyd(Integer origen, Integer destino) throws Exception {
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        Double[][] distancias = adyacencia();
        Integer[][] recor = new Integer[nVertices + 1][nVertices + 1];
        Integer[][] ruta = new Integer[nVertices + 1][nVertices + 1];
        
        for (int i = 1; i <= nVertices; i++) {
            for (int j = 1; j <= nVertices; j++) {
                ruta[i][j] = j;
                recor[i][j] = i;
                if (i == j) {
                    ruta[i][j] = 0;
                    recor[i][j] = 0;
                }
            }
        }
        
        for (int k = 1; k <= nVertices; k++) {
            for (int i = 1; i <= nVertices; i++) {
                for (int j = 1; j <= nVertices; j++) {
                    if (distancias[i][k] + distancias[k][j] < distancias[i][j]) {
                        distancias[i][j] = distancias[i][k] + distancias[k][j];
                        ruta[i][j] = recor[k][j];
                    }
                }
            }
        }
        
        Integer i = origen;
        Integer j = destino;

        while (i != j) {
            camino.insertarCabecera(i);
            i = ruta[i][j];
        }

        camino.insertarCabecera(i);
        return camino;
    }

    public ListaEnlazada<Integer> djikstra(Integer origen, Integer destino) throws Exception {
        ListaEnlazada<Integer> camino = new ListaEnlazada<>();
        Integer[][] cv = new Integer[nVertices + 1][nVertices + 1];
        Integer[][] mc = new Integer[nVertices + 1][nVertices + 1];
        Double[][] md = new Double[nVertices + 1][nVertices + 1];
        pesos(cv, mc, md);

        for (int i = 1; i <= nVertices(); i++) {
            for (int j = 1; j <= nVertices(); j++) {
                for (int k = 1; k <= nVertices(); k++) {
                    if (md[j][i] + md[i][k] < md[j][k]) {
                        md[j][k] = md[j][i] + md[i][k];
                        cv[j][k] = mc[i][k];
                    }
                }
            }
        }
        while (origen != destino) {
            camino.insertarCabecera(origen);
            origen = cv[origen][destino];
        }
        camino.insertarCabecera(origen);
        return camino;
    }

    public void pesos(Integer[][] cv, Integer[][] mc, Double[][] md) throws Exception {
        for (int i = 1; i <= nVertices(); i++) {
            for (int j = 1; j <= nVertices(); j++) {
                cv[i][j] = j;
                mc[i][j] = i;

                if (i == j) {
                    md[i][j] = 0.0;
                    cv[i][j] = 0;
                    mc[i][j] = 0;
                } else {
                    md[i][j] = ((!(Boolean) existeArista(i, j)[0]) ? Double.POSITIVE_INFINITY : pesoArista(i, j));
                }
            }
        }
    }

    public Integer[] anchura(Integer origen) throws Exception {
        int i = 0;
        boolean conf = false;
        Integer[] visto = new Integer[nVertices];
        Integer cont = 1;
        visto[0] = origen;
        while (cont < nVertices) {
            i++;
            for (int j = 0; j < listaAdyacente[origen].getSize(); j++) {
                for (int k = 0; k < visto.length; k++) {
                    if (visto[k] == listaAdyacente[origen].obtener(j).getDestino()) {
                        conf = true;
                        break;
                    } else {
                        conf = false;
                    }
                }
                if (!conf) {
                    visto[cont] = listaAdyacente[origen].obtener(j).getDestino();
                    cont++;
                }
            }
            if (origen == nVertices) {
                origen = 0;
            }
            if (cont == nVertices) {
                break;
            }
            origen++;
            if (i > nVertices) {
                break;
            }
        }
        return visto;
    }

    public Integer[] profundidad(Integer origen) throws Exception {
        Integer res[] = new Integer[nVertices() + 1];
        visitados = new Integer[nVertices() + 1];
        ordenVisita = 0;

        for (int i = 0; i <= nVertices(); i++) {
            visitados[i] = 0;
        }
        for (int i = 1; i <= nVertices(); i++) {
            if (visitados[i] == 0) {
                toArrayA(i, res);
            }
        }
        Integer[] v = res;
        for (int j = 0; j < res.length; j++) {
            for (int k = 0; k < v.length; k++) {
                if (j != k) {
                    if (res[j] == v[k]) {
                        for (int i = k; i < v.length - 1; i++) {
                            res[i] = v[i + 1];
                        }
                    }
                }
            }
        }
        return res;
    }

    public void toArrayA(Integer origen, Integer res[]) throws Exception {
        res[ordenVisita] = origen;
        visitados[origen] = ordenVisita++;
        ListaEnlazada<Adyacencia> obj = listaAdyacente[origen];
        for (int i = 0; i < obj.getSize(); i++) {
            Adyacencia ad;
            if (obj.comprobarDato(i)) {
                break;
            } else {
                ad = obj.obtener(i);
            }
            obj.setCabecera(obj.getCabecera().getSiguiente());
            if (visitados[ad.getDestino()] == 0) {
                toArrayA(ad.getDestino(), res);
            }
        }
    }
}
