package Controlador.TDA_Grafo;

import Controlador.Exceptions.VerticeException;

/**
 *
 * @author Leo117
 */
public class GrafoND extends GrafoD{

    public GrafoND(Integer numV) {
        super(numV);
    }
    
    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if (i <= getnVertices() && j <= getnVertices()) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                nAristas++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adyacencia(i, peso));
            }
        } else {
            throw new VerticeException("Cierto vertice ingresado no existe");
        }
    }
}
