package Controlador.TDA_Grafo;

import Controlador.Exceptions.VerticeException;

/**
 *
 * @author Leo117
 */
public class GrafoDNE<E> extends GrafoDE<E> {

    public GrafoDNE(Integer nVertices, Class clazz) {
        super(nVertices, clazz);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if ( i <= getnVertices() && j <= getnVertices()) {
            Object[] existe = existeArista(i, j);
            if (!((Boolean) existe[0])) {
                nAristas++;
                listaAdyacente[i].insertarCabecera(new Adyacencia(j, peso));
                listaAdyacente[j].insertarCabecera(new Adyacencia(i, peso));
            }
        } else {
            throw new VerticeException("Algun vertice ingresado no existe");
        }
    }

    @Override
    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
        insertarArista(obtenerCodigo(j), obtenerCodigo(i), peso);
    }
}
