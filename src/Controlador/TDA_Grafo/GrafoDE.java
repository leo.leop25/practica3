package Controlador.TDA_Grafo;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Lista.ListaEnlazada;
import java.util.HashMap;

/**
 *
 * @author Leo117
 */
public class GrafoDE<E> extends GrafoD {

    protected Class<E> clazz;
    protected E etiquetas[];
    protected HashMap<E, Integer> dicVertices;

    public GrafoDE(Integer numV, Class clazz) {
        super(numV);
        this.clazz = clazz;
        etiquetas = (E[]) java.lang.reflect.Array.newInstance(this.clazz, numV + 1);
        dicVertices = new HashMap<>(numV);
    }

    public Object[] existeAristaE(E i, E j) throws Exception {
        return this.existeArista(obtenerCodigo(i), obtenerCodigo(j));
    }

    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
    }

    public void insertarAristaE(E i, E j) throws Exception {
        this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), Double.NaN);
    }

    public Integer obtenerCodigo(E etiqueta) throws Exception {
        Integer key = dicVertices.get(etiqueta);
        if (key != null) {
            return key;
        } else {
            throw new VerticeException("NO SE ENCUENTRA ETIQUETA CORRESPONDIENTE");
        }
    }

    public E obtenerEtiqueta(Integer codigo) throws Exception {
        return etiquetas[codigo];
    }

    public ListaEnlazada<Adyacencia> adyacentesDEE(E i) throws Exception {
        return adyacente(obtenerCodigo(i));
    }

    public void etiquetarVertice(Integer codigo, E etiqueta) {
        etiquetas[codigo] = etiqueta;
        dicVertices.put(etiqueta, codigo);
    }

    public void nuevoGrafoE() {
        super.nuevoGrafo();
        E aux[] = (E[]) java.lang.reflect.Array.newInstance(this.clazz, nVertices + 1);
        HashMap<E, Integer> nuevoDic = new HashMap<>(nVertices);
        for (int i = 0; i < etiquetas.length; i++) {
            aux[i] = etiquetas[i];
            nuevoDic.put(aux[i], i);
        }
        etiquetas = aux;
        dicVertices = nuevoDic;
    }

    @Override
    public String toString() {
        StringBuffer grafo = new StringBuffer("");
        try {
            for (int i = 1; i <= nVertices(); i++) {
                grafo.append(">> " + i + " -- " + obtenerEtiqueta(i).toString());
                try {
                    ListaEnlazada<Adyacencia> lista = adyacente(i);

                    for (int j = 0; j < lista.getSize(); j++) {
                        Adyacencia aux = lista.obtener(j);
                        if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                            grafo.append(" --- DESTINO " + aux.getDestino() + " -- " + obtenerEtiqueta(aux.getDestino()));
                        } else {
                            grafo.append(" --- DESTINO " + aux.getDestino() + " -- " + obtenerEtiqueta(aux.getDestino()) + " -peso- " + aux.getPeso());
                        }
                    }
                    grafo.append("\n");
                } catch (Exception e) {
                    System.out.println("Error en toString " + e);
                }
            }
        } catch (Exception e) {
            System.out.println("Error en toString" + e);
        }
        return grafo.toString();
    }
}
