package Controlador.TDA_Grafo;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Lista.ListaEnlazada;

/**
 *
 * @author Leo117
 */
public abstract class Grafo {

    public Integer visitados[];

    public Integer ordenVisita;

    public ListaEnlazada<Integer> q;
    
    public abstract Integer nVertices();

    public abstract Integer nAristas();

    public abstract Object[] existeArista(Integer i, Integer f) throws VerticeException;

    public abstract Double pesoArista(Integer i, Integer f) throws VerticeException;

    public abstract void insertarArista(Integer i, Integer j) throws VerticeException;

    public abstract void insertarArista(Integer i, Integer j, Double peso) throws VerticeException;

    public abstract ListaEnlazada<Adyacencia> adyacente(Integer i) throws VerticeException;

    @Override
    public String toString() {
        StringBuffer grafo = new StringBuffer("");
        for (int i = 1; i <= nVertices(); i++) {
            try {
                grafo.append("VERTICE " + i);
                ListaEnlazada<Adyacencia> lista = adyacente(i);
                for (int j = 0; j < lista.getSize(); j++) {
                    try {
                        Adyacencia obj = lista.obtener(j);
                        if (obj.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                            grafo.append("---- DESTINO " + obj.getDestino());
                        }else{
                            grafo.append("--- DESTINO " + obj.getDestino()+"------- peso---"+obj.getPeso());
                        }
                        
                    } catch (Exception ex) {
                    }
                }
                grafo.append("\n");
            } catch (VerticeException ex) {
                System.out.println("ERROR: " + ex);
            }
        }
        return grafo.toString();
    }
}
