package Controlador.LugarController;

import Controlador.TDA_Grafo.GrafoDNE;
import Modelo.Localidad;

/**
 *
 * @author Leo117
 */
public class LocalidadController {

    private GrafoDNE<Localidad> grafoL;
    private Localidad locate;
    int n = 1;

    public LocalidadController() {

    }
    public void agregarGr(Localidad locate) {
        grafoL.nuevoGrafoE();
        grafoL.etiquetarVertice(n, locate);
        n++;
    }

    public Double calcularDistancia(Localidad punto1, Localidad punto2) {
        
        Double or = punto1.getLatitud() - punto2.getLongitud();
        Double ds = punto1.getLatitud() - punto2.getLongitud();
        Double ruta = 0.0;
        ruta = Math.sqrt((or * or) + (ds * ds));
        return Math.round(ruta) *100.0 /100.0;
        //return ruta;
    }

    public GrafoDNE<Localidad> getGrafoL() {
        return grafoL;
    }

    public void setGrafoL(GrafoDNE<Localidad> grafoL) {
        this.grafoL = grafoL;
    }
    
    public GrafoDNE<Localidad> getGrafoLocate() {
        if (grafoL == null) {
            grafoL = new GrafoDNE<>(0, Localidad.class);
        }
        return grafoL;
    }

    public void setGrafoLocate(GrafoDNE<Localidad> grafoL) {
        this.grafoL = grafoL;
    }

    public Localidad getLocate() {
        if (locate == null) {
            locate = new Localidad();
        }
        return locate;
    }

    public void setLocate(Localidad locate) {
        this.locate = locate;
    }

    
}
