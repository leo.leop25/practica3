
package Controlador.Exceptions;

/**
 *
 * @author Leo117
 */
public class EmptyListException extends Exception {

    public EmptyListException() {
    }

    public EmptyListException(String msg) {
        super(msg);
    }
}
