
package Controlador.Exceptions;

/**
 *
 * @author Leo117
 */
public class ListOutLimitException extends Exception {

    public ListOutLimitException() {
    }

    public ListOutLimitException(String msg) {
        super(msg);
    }
}
