
package Modelo;

/**
 *
 * @author Leo117
 */
public class Localidad {
    private String ciudad;
    private String provincia;
    private Double latitud;
    private Double longitud;


    public Localidad(String ciudad, String provincia, Double latitud, Double longitud) {
        this.ciudad = ciudad;
        this.provincia = provincia;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Localidad() {
    }
    
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    
    
    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString(){
        return ciudad +"-"+ provincia;
    }
}
