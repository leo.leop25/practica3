package Vista.Tablas;

import Controlador.Exceptions.VerticeException;
import Controlador.TDA_Grafo.GrafoDNE;
import Modelo.Localidad;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Leo117
 */
public class TablaAristas extends AbstractTableModel {

    private GrafoDNE <Localidad> grafoEND;
    private String[] columnas;

    
    public GrafoDNE getGrafoEND() {
        return grafoEND;

    }
    
    public void setGrafoEND(GrafoDNE grafoEND){
        this.grafoEND = grafoEND;
    }
    
    public String[] getColumnas() {
        return columnas;
    }

    public void setColumnas(String[] columnas) {
        this.columnas = columnas;
    }
    
    @Override
    public int getRowCount() {
        return grafoEND.nVertices();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "Ciudad";
            case 1:
                return "Provincia";
            case 2:
                return "Latitud";
            case 3:
                return "Longitud";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        try{
            Localidad locate = grafoEND.obtenerEtiqueta(arg0+1);
            switch(arg1){
                case 0:
                    return locate.getCiudad();
                case 1:
                    return locate.getProvincia();
                case 2:
                    return locate.getLatitud();
                case 3:
                    return locate.getLongitud();
                default:
                    return null;
            }
        }catch(Exception e){
            System.out.println("Error en cargar tabla");
            return null;
        }
    }
}
