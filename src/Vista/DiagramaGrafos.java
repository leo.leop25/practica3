package Vista;

import Controlador.TDA_Grafo.Adyacencia;
import Controlador.TDA_Grafo.GrafoDNE;
import Controlador.TDA_Lista.ListaEnlazada;
import Modelo.Localidad;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import com.mxgraph.view.mxGraph;
import java.awt.Dimension;
import javax.swing.JDialog;

/**
 *
 * @author Leo117
 */
public class DiagramaGrafos extends JDialog {

    private GrafoDNE<Localidad> gf;
    ListaEnlazada<Object[]> listaObj = new ListaEnlazada<>();
    Object ob = null;

    mxGraph mx = new mxGraph();
    mxGraphComponent graphComponent = new mxGraphComponent(mx);
    Object parent = mx.getDefaultParent();
    mxIGraphLayout lay = new mxFastOrganicLayout(mx);

    public DiagramaGrafos() {
        this.setModal(true);
        setLocationRelativeTo(this);
        setResizable(true);
    }

    public DiagramaGrafos(GrafoDNE<Localidad> gf) {
        this.setModal(true);
        this.gf = gf;
        generar();
    }
    
    private void generar() {

        graphComponent.setSize(new Dimension(700, 400));
        mx.getModel().beginUpdate();
        try {
            for (int i = 1; i <= this.gf.nVertices(); i++) {
                
                Object a = mx.insertVertex(parent, (this.gf.obtenerEtiqueta(i).getCiudad())
                                                 , (this.gf.obtenerEtiqueta(i).getCiudad())
                                                 , 100, 100, 100, 50);
                Object[] aux = {i, a};
                listaObj.insertarCabecera(aux);
            }
            
            for (int i = 1; i <= this.gf.nVertices(); i++) {
                ListaEnlazada<Adyacencia> lista = gf.adyacente(i);
                Object a = cargarGr(i, listaObj);
                for (int j = 0; j < lista.getSize(); j++) {
                    Adyacencia aux = lista.obtener(j);
                    Object b = cargarGr(aux.getDestino(), listaObj);
                    mx.insertEdge(parent, null, aux.getPeso().toString(), a, b);
                }
            }
        } catch (Exception e) {
        } finally {
            mx.getModel().endUpdate();
        }
        animacionGrafos(mx, graphComponent);
        new mxHierarchicalLayout(mx).execute(mx.getDefaultParent());
        getContentPane().add(graphComponent);
    }
    

    private Object cargarGr(Integer vertice, ListaEnlazada<Object[]> lista) {
        try {
            for (int i = 0; i < lista.getSize(); i++) {
                Object[] aux = lista.obtener(i);
                if (((Integer) aux[0]).intValue() == vertice.intValue()) {
                    ob = aux[1];
                    break;
                }
            }
            return ob;
        } catch (Exception e) {
            System.out.println("Error no se encontro " + e);
            return null;
        }
    }

    private void animacionGrafos(mxGraph mx, mxGraphComponent mxc) {
        mx.getModel().beginUpdate();
        try {
            lay.execute(mx.getDefaultParent());
        } finally {
            terminacion(mxc);
        }
    }
    
    private void terminacion(mxGraphComponent mxc){
        mxMorphing morp = new mxMorphing(mxc, 30, 2.0, 30);
            morp.addListener(mxEvent.DONE, new mxEventSource.mxIEventListener() {
            
                @Override
                public void invoke(Object arg0, mxEventObject arg1) {
                    mx.getModel().endUpdate();
                }
            });
            morp.startAnimation();
    }

}
